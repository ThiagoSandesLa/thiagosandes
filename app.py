import csv


def calcularImc(peso, altura):
    peso = float(peso.replace(',', '.') or 0)
    altura = float(altura.replace(',', '.') or 1)
    return '{:.2f}'.format(peso/(altura**2)).replace('.', ',')

def formatarTexto(nome):
    return ' '.join(nome.upper().strip().split())

def ler():
    with open('./dataset.CSV', 'r', encoding='ISO-8859-1') as arquivo:
        dataset = list(csv.reader(arquivo, delimiter=';'))
        for linha in dataset[1:]:
            nome = formatarTexto(linha[0])
            sobrenome = formatarTexto(linha[1])
            valor = calcularImc(linha[2], linha[3])
            linha = nome + ' ' + sobrenome + ' ' + valor
            print(linha)
            yield linha

def escrever(conteudo):
    with open('./ThiagoSandes.txt', 'w') as arquivo:
        for linha in conteudo:
            arquivo.write(linha + '\n')
        arquivo.close()


conteudo = ler()
escrever(conteudo)